// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VModal from 'vue-js-modal'
import ToggleButton from 'vue-js-toggle-button'
import VueSocketio from 'vue-socket.io'
import Notifications from 'vue-notification'
import VueStripeCheckout from 'vue-stripe-checkout'
import socketio from 'socket.io-client'
import vSelect from 'vue-select'

import router from './router'
import store from './store'
import './filters'
import App from './App.vue'
import MenuBar from '@/components/MenuBar'
import ErrorHandler from '@/libs/ErrorHandler'
import BaseCollapse from '@/components/BaseCollapse'
import PurchaseHistory from '@/components/PurchaseHistory'
import InstantPurchase from '@/components/InstantPurchase'
import PaymentMethod from '@/components/PaymentMethod'

Vue.use(new VueSocketio({
  connection: socketio(`${process.env.VUE_APP_SOCKER_URL}`, { transports: ['websocket'] })
}))
Vue.use(Notifications)
Vue.use(ToggleButton)
Vue.use(VModal)
Vue.use(VueStripeCheckout, {
  key: `${process.env.VUE_APP_STRIPE_KEY}`,
  image: require('@/assets/logo-square.jpg'),
  currency: 'usd',
  amount: 5000,
  name: 'Zero Flex',
  description: 'Monthly subscription'
})

Vue.component('menu-bar', MenuBar)
Vue.component('v-select', vSelect)
Vue.component('base-collapse', BaseCollapse)
Vue.component('purchase-history', PurchaseHistory)
Vue.component('instant-purchase', InstantPurchase)
Vue.component('payment-method', PaymentMethod)

Vue.prototype.$eventBus = new Vue()
ErrorHandler.WrapEmit(Vue)

/* eslint-disable no-new */
const vue = new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})

router.beforeResolve((to, from, next) => {
  if (vue.$store.getters.userStatus === 'inactive' && to.name !== 'InActive') {
    next({ name: 'InActive' })
  } else if (['waiting', 'expired'].includes(vue.$store.getters.userStatus) &&
    !['WaitingList', 'Subscription'].includes(to.name)) {
    next({ name: 'WaitingList' })
  } else if (['Home', 'Notification', 'AdvanceSettings', 'Subscription'].includes(to.name) &&
    !vue.$store.getters.isZeroTokenExisted) {
    next({ name: 'Login' })
  } else if (['Login', 'SignUp'].includes(to.name) && vue.$store.getters.isZeroTokenExisted) {
    next({ name: 'Home' })
  } else {
    next()
  }
})
