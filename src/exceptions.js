export class Exception {
  constructor (title = '', description = '') {
    this.title = title
    this.description = description
  }
}

export class InvalidEmailException extends Exception {
  constructor () {
    super('Email invalid')
  }
}

export class InvalidPasswordException extends Exception {
  constructor () {
    super('Password invalid')
  }
}
