import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/views/Login.vue'
import Home from '@/views/Home.vue'
import Notification from '@/views/Notification.vue'
import AdvanceSettings from '@/views/AdvanceSettings.vue'
import SignUp from '@/views/SignUp.vue'
import WaitingList from '@/views/WaitingList.vue'
import Subscription from '@/views/Subscription.vue'
import RefreshToken from '@/views/RefreshToken.vue'
import InActive from '@/views/InActive'

Vue.use(Router)

const router = new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/notification',
      name: 'Notification',
      component: Notification
    },
    {
      path: '/advance-settings',
      name: 'AdvanceSettings',
      component: AdvanceSettings
    },
    {
      path: '/signup',
      name: 'SignUp',
      component: SignUp
    },
    {
      path: '/waitinglist',
      name: 'WaitingList',
      component: WaitingList
    },
    {
      path: '/subscription',
      name: 'Subscription',
      component: Subscription
    },
    {
      path: '/refreshtoken',
      name: 'RefreshToken',
      component: RefreshToken
    },
    {
      path: '/inactive',
      name: 'InActive',
      component: InActive
    }
  ]
})

export default router
