import Vue from 'vue'
import moment from 'moment'

Vue.filter('fromUtcToLocal', function (utcDate) {
  let utcObject = moment.utc(utcDate)
  return utcObject.local().format('MMM DD YYYY')
})

Vue.filter('utcFormat', function (utcDate) {
  let utcObject = moment.utc(utcDate)
  return utcObject.format('MMM DD YYYY Z')
})

Vue.filter('yesNoFormat', function (booleanValue) {
  return booleanValue ? 'yes' : 'no'
})

Vue.filter('formatBrandName', function (brand) {
  switch (brand.toLowerCase()) {
    case 'american express':
      return 'Amex'
    case 'diners club':
      return 'Diner'
    default:
      return brand
  }
})

Vue.filter('date', function (date, format = 'YY-MM-DD') {
  let dateObject = moment(date)
  return dateObject.format(format)
})
