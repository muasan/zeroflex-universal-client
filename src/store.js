import Vuex from 'vuex'
import Vue from 'vue'
import createPersistedState from 'vuex-persistedstate'
import * as _ from 'lodash'
import moment from 'moment'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    user: null,
    stations: [],
    activityLog: [],
    zeroToken: '',
    paymentMethods: []
  },

  getters: {
    email: (state) => { return state.user !== null && state.user.email },
    expiredDate: (state) => { return state.user !== null && state.user.expired_date },
    stationList: (state) => { return state.stations },
    isUserNotExisted: (state) => { return state.user === null },
    activityLog: (state) => { return state.activityLog },
    zeroToken: (state) => { return state.zeroToken },
    isZeroTokenExisted: (state) => { return Boolean(state.zeroToken) },
    isInWaitingList: (state) => { return Boolean(state.user) && Boolean(state.user.is_waiting) },
    isSubscribed: (state) => { return Boolean(state.user) && Boolean(state.user.is_subscribed) },
    hasPaymentMethods: (state) => { return state.paymentMethods.length > 0 },
    paymentMethods: (state) => { return state.paymentMethods },
    userStatus: (state) => {
      if (state.user) {
        if (state.user['is_active']) {
          if (state.user['is_waiting']) { return 'waiting' }
          let expiredDate = moment.utc(state.user['expired_date'])
          if (expiredDate < moment()) { return 'expired' }
          return 'active'
        } else { // inactive
          return 'inactive'
        }
      }
    }
  },

  mutations: {
    updateUser (state, user) {
      if (user !== null) {
        if (state.user === null) {
          state.user = {}
        }
        state.user = { ...state.user, ...user }
      } else {
        state.user = null
      }
    },

    updateStationList (state, stationList) {
      state.stations = stationList
    },

    updateBotStatus (state, status) {
      state.botStatus = status
    },

    backupActivityLog (state, log) {
      state.activityLog = log
    },

    insertLogLine (state, logLine) {
      state.activityLog.push(logLine)
    },

    updateZeroToken (state, token) {
      state.zeroToken = token
    },

    updatePaymentMethods (state, methods) {
      state.paymentMethods = methods
    },

    addPaymentMethod (state, method) {
      if (_.findIndex(state.paymentMethods, m => m['id'] === method['id']) === -1) {
        state.paymentMethods.push(method)
      }
    },

    removePaymentMethod (state, method) {
      let index = _.findIndex(state.paymentMethods, m => m['id'] === method['id'])
      if (index !== -1) {
        state.paymentMethods.splice(index, 1)
      }
    },

    updateSubscriptionStatus (state, status) {
      state.user['is_subscribed'] = Boolean(status)
    }
  },

  actions: {
    async clearUser ({ commit }) {
      commit('updateUser', null)
    },

    async clearActivityLog ({ commit }) {
      await commit('backupActivityLog', [])
    },

    async clearZeroToken ({ commit }) {
      await commit('updateZeroToken', '')
    },

    async insertMessage ({ commit, state }, message) {
      state.activityLog.push(message)
    }
  },

  plugins: [createPersistedState({
    paths: ['zeroToken', 'activityLog']
  })]
})

export default store
