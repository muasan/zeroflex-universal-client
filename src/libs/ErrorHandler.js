import * as _ from 'lodash'

export default class ErrorHandler {
  #eventName;
  #errorObject;
  constructor (eventName, errorObject = {}) {
    this.#eventName = eventName
    if (typeof errorObject === 'string') {
      let detail = errorObject
      errorObject = { description: detail }
    }
    this.#errorObject = errorObject
  }

  get title () {
    switch (this.#eventName) {
      case 'update-settings-failed':
        return 'Update settings failed'
      case 'load-settings-failed':
        return 'Load settings failed'
      case 'load-station-list-failed':
        return 'Load station list failed'
      case 'load-user-profile':
        return 'Load user profile failed'
      case 'login-failed':
        return 'Login failed'
      case 'bad-access-token':
        return 'Bad access token'
      case 'start-bot-failed':
        return 'Start bot failed'
      case 'stop-bot-failed':
        return 'Stop bot failed'
      default:
        return this.#eventName
    }
  }

  get detail () {
    if (this.isAXIOSError()) {
      return this.getAXIOSErrorDetail()
    } else if (_.hasIn(this.#errorObject, 'description')) {
      return this.#errorObject.description
    } else if (_.hasIn(this.#errorObject, 'title')) {
      return this.#errorObject.title
    } else if (_.hasIn(this.#errorObject, 'message')) {
      return this.#errorObject.message
    } else {
      console.error(this.#errorObject)
      return 'Unknown'
    }
  }

  get AxiosTitle () {
    if (this.isAXIOSError()) {
      let error = this.#errorObject
      return error.data.title
    }
    return null
  }

  isAXIOSError () {
    return _.hasIn(this.#errorObject, ['data'])
  }

  getAXIOSErrorDetail () {
    let error = this.#errorObject
    return error.data.description || error.data.title || (typeof error.data === 'string' ? error.data : '')
  }

  static WrapEmit (vueClass) {
    let oldEmit = vueClass.prototype.$eventBus.$emit
    vueClass.prototype.$eventBus.$emit = function (event, ...args) {
      let errorPattern = /^error-(.+)/
      let match = event.match(errorPattern)
      if (match && match[1]) {
        oldEmit.call(this, 'error', match[1], ...args)
      } else {
        oldEmit.call(this, event, ...args)
      }
    }
  }
}
