import axios from 'axios'

const API_HOST = `${process.env.VUE_APP_API_URL}`

function handleError (error) {
  if (error.response) {
    // eslint-disable-next-line
    return Promise.reject(error.response)
  } else if (error.request) {
    // eslint-disable-next-line
    return Promise.reject({
      data: {
        description: 'Server down'
      }
    })
  }
}

// Add a response interceptor
axios.interceptors.response.use(
  function (response) {
    // Do something with response data
    if (response.data) {
      return response.data
    } else {
      return response
    }
  },
  function (error) {
    // Do something with response error
    return handleError(error)
  }
)

export default {
  login (email, password) {
    let url = API_HOST + '/user/login'
    let data = {
      email: email,
      password: password
    }
    return axios.post(url, data)
      .then(function (data) {
        return data.session
      })
  },

  logout () {
    let url = API_HOST + '/user/logout'
    return axios.post(url)
  },

  profile (sessionId) {
    let url = API_HOST + '/user/profile'
    return axios.get(url)
  },

  startBot () {
    let url = API_HOST + '/bot'
    let params = { action: 'start' }
    return axios.post(url, {}, { params: params })
  },

  stopBot () {
    let url = API_HOST + '/bot'
    let params = { action: 'stop' }
    return axios.post(url, {}, { params: params })
  },

  fetchBotSettings () {
    let url = API_HOST + '/bot/settings'
    return axios.get(url)
  }, // <== fetchBotSettings

  inquireBotStatus () {
    let url = API_HOST + '/bot'
    return axios.get(url)
  }, // <== inquireBotStatus

  fetchStationList () {
    let url = API_HOST + '/stations'
    return axios.get(url)
  }, // <== fetchStationList

  updateBotSettings (settings) {
    let url = API_HOST + '/bot/settings'
    return axios.post(url, settings)
  }, // <== updateBotSettings

  fetchNotifications () {
    let url = API_HOST + '/notification'
    return axios.get(url)
  }, // <== fetchNotifications

  informBotSettingsChanged () {
    let url = API_HOST + '/bot'
    return axios.put(url)
  }, // <== informBotSettingsChanged

  fetchNotificationSettings () {
    let url = API_HOST + '/notification'
    return axios.put(url)
  }, // <== fetchNotificationSettings

  updateNotificationSettings (settings) {
    let url = API_HOST + '/notification'
    return axios.post(url, settings)
  }, // <== updateNotificationSettings

  fetchProfile () {
    let url = API_HOST + '/user/profile'
    return axios.get(url)
  }, // <== fetchProfile

  signup (email, password, payload = null) {
    let url = API_HOST + '/user/signup'
    let data = {
      'email': email,
      'password': password
    }
    if (payload) {
      data['payload'] = payload
    }
    return axios.post(url, data)
  }, // <== signup

  purchase (token) {
    let url = API_HOST + '/subscription/purchase'
    let data = {
      token: token.id,
      email: token.email
    }
    return axios.post(url, data)
  }, // <== purchase

  fetchPaymentMethod () {
    let url = API_HOST + '/payment_method'
    return axios.get(url)
  },

  subscribe (token) {
    let url = API_HOST + '/subscription/subscribe'
    let data = {
      token: token.id
    }
    return axios.post(url, data)
  },

  unsubscribe () {
    let url = API_HOST + '/subscription/subscribe'
    return axios.put(url, { status: false })
  },

  deletePaymentMethod (paymentMethodId) {
    let url = API_HOST + '/payment_method'
    let data = {
      payment_method_id: paymentMethodId
    }
    return axios.delete(url, { params: data })
  },

  createPaymentMethod (token) {
    let url = API_HOST + '/payment_method'
    let data = {
      token: token.id
    }
    return axios.post(url, data)
  },

  fetchDefaultPaymentMethod () {
    let url = API_HOST + '/payment_method/default'
    return axios.get(url)
  },

  updateDefaultPaymentMethod (paymentMethodId) {
    let url = API_HOST + '/payment_method/default'
    let data = {
      'payment_method_id': paymentMethodId
    }
    return axios.post(url, data)
  },

  fetchPurchaseHistory () {
    let url = API_HOST + '/purchases'
    return axios.get(url)
  },

  renewRefreshToken (password, payload) {
    let url = API_HOST + '/token/renew'
    let data = {
      'password': password
    }
    if (payload) {
      data['payload'] = payload
    }
    return axios.post(url, data)
  },

  fetchPriorityInWaitingList () {
    let url = API_HOST + '/waitinglist'
    return axios.get(url)
  },

  optInWaitingList () {
    let url = API_HOST + '/waitinglist'
    let data = { 'opt_in': true }
    return axios.post(url, data)
  },

  optOutWaitingList () {
    let url = API_HOST + '/waitinglist'
    let data = { 'opt_in': false }
    return axios.post(url, data)
  },

  SET_ZERO_TOKEN (zeroToken) {
    axios.defaults.headers.common['ZERO-TOKEN'] = zeroToken
  }
}
