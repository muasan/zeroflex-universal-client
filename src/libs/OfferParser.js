import moment from 'moment'

export function parseOfferList (notifications, stationList, caller) {
  for (let notification of notifications) {
    let event = notification.event
    if (event === 'Block filtered' || event === 'Block accepted' || event === 'Block missed') {
      let message = _extract_body_from_notification(notification, stationList)
      caller({
        body: message,
        created: notification.created
      })
    }
  }
}// <== _render_notifications

function _extract_body_from_notification (notification, stationList) {
  let body = notification.event
  if (notification.event === 'Block accepted' || notification.event === 'Block filtered' || notification.event === 'Block missed') {
    try {
      let offer = notification.message
      let beginTime = moment(offer.startTime * 1000)
      let endTime = moment(offer.endTime * 1000)
      let beginHour = beginTime.format('hh:mma')
      let endHour = endTime.format('hh:mma')
      let dateOfWeek = _convertDateToDateOfWeek(beginTime)
      let areaName = _getAreaNameById(offer.serviceAreaId, stationList)

      body += `: ${beginHour}-${endHour} [${dateOfWeek}] [${areaName}]`
    } catch (err) {
      console.log(err)
    }
  }

  return body
} // <== _extract_body_from_notification

function _convertDateToDateOfWeek (date) {
  if (moment().isSame(date, 'day')) {
    return 'Today'
  } else if (moment().add(1, 'days').isSame(date, 'day')) {
    return 'Tomorrow'
  } else {
    return date.format('MMM DD')
  }
}

function _getAreaNameById (areaId, stationList) {
  let areaName = 'Unknown'
  for (let station of stationList) {
    if (station.id === areaId) {
      return station.name
    }
  }
  return areaName
} // <== _getAreaNameById
