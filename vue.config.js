module.exports = {
  css: {
    loaderOptions: {
      sass: {
        data: `
          @import "@/assets/styles/normalize.scss";
          @import "@/assets/styles/variables.scss";
          @import "@/assets/styles/cross-browser-mixins.scss";
          @import "@/assets/styles/mixins.scss";
          @import "@/assets/styles/animations.scss";
          @import "./node_modules/hamburgers/_sass/hamburgers/hamburgers.scss";
        `
      }
    }
  },
  runtimeCompiler: true,
  configureWebpack: {
    devtool: 'source-map'
  },
  outputDir: `${process.env.VUE_APP_OUTPUT || 'dist'}`
};
